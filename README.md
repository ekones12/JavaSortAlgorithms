In this homework, we categorize the contents of the csv file expected by us with the
selected sorting algorithm according to the given criterion. And to calculate the time
complexity values of the algorithms we use and to show performance analysis of the
algorithms.

The algorithms to be analyzed are :
        
        1. Insertion sort algortihm
        2. Selection sort algorithm
        3. Bubble sort algorithm