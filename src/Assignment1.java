import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Assignment1 {

	
	public static ArrayList<list> twoarray = new ArrayList<list>();
	public static void reader (String args1 ) {
			
	        BufferedReader br = null;
	        String line = "";
	        String cvsSplitBy = ",";
	        int lines=0;
	        try {
	            br = new BufferedReader(new FileReader(args1));
	            while ((line = br.readLine()) != null) {
	                String[] country = line.split(cvsSplitBy); 
	                if(lines == 0) {
	                	 twoarray.add(new list(country,1));
	                }
	                else
	                twoarray.add(new list(country,2));
	                lines++;
	            }

	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            if (br != null) {
	                try {
	                    br.close();
	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	            }
	        }
	}
	
	
	public static void insertionSort(String args1, ArrayList<list> numbers1, String n,String tf)
	{
	ArrayList<list> numbers = new ArrayList<list>(numbers1);
	Collections.copy(numbers, numbers1);
	int index = Integer.parseInt(n);
	int length=numbers.size();
	   int i, j;
	   double key;
	   long first =  System.currentTimeMillis();
	   for (i = 2; i < length; i++)
	   {
	       key = (double) numbers.get(i).intlist.get(index);
	       j = i-1;
	    
	 
	       while (j >= 1 && (double) numbers.get(j).intlist.get(index) > key)
	       {
	    	   Collections.swap(numbers,j+1,j);
	           j = j-1;
	       }
	    
	       
	   }
	   long last =  System.currentTimeMillis()-first;
	   System.out.println("--> Insertion Sort Algotihm \n--> Milisecond : "+last +" \n--> Second : "+last/1000+"\n--> Minute : "+last/60000+"\n-------------------------");

	   if (tf .equals("T")) {
		   FileWriter writer  = null;
		        try {
		        writer = new FileWriter(args1);
		        String NEW_LINE_SEPARATOR = "\n";
		        String COMMA_DELIMITER = ",";
		        int a;
		        for(a = 0 ;a < numbers.get(0).intlist.size();a++) {
		        	writer.append(numbers.get(0).intlist.get(a).toString());
		        	//System.out.println(numbers.get(0).intlist.get(a).toString());
		        	writer.append(COMMA_DELIMITER);
		        }
		        writer.append(NEW_LINE_SEPARATOR);
		    int i1 ,j1;
		    int k = numbers.size();
		    for(i1=1;i1<k;i1++) {
		    	int k1= numbers.get(i1).intlist.size();
		    	for(j1=0;j1<k1;j1++) {
			        writer.append(numbers.get(i1).intlist.get(j1).toString());
			        writer.append(COMMA_DELIMITER);
		    	}
		    	writer.append(NEW_LINE_SEPARATOR);
		    }
		           
		         } catch (Exception e) {
		             System.out.println("Error in CsvFileWriter !!!");
		              e.printStackTrace();
		           } 
		        finally {
		        	            try {
		        	
		        	            	writer.flush();
		        	
		        	                writer.close();
		        	
		        	            } catch (IOException e) {
		        	
		        	                System.out.println("Error while flushing/closing fileWriter !!!");
		        	
		        	                e.printStackTrace();
		        	
		        	            }
		        	
		        	
		        	        }
	   }
	   
	}
		        
		        	             

	
       
	
	 public static void SelectionSort(ArrayList<list> numbers1,String args){
		 	ArrayList<list> numbers = new ArrayList<list>(numbers1);
			Collections.copy(numbers, numbers1);
			int lenght= numbers.size();
		 	int indexfeature = Integer.parseInt(args);
		 	long first =  System.currentTimeMillis();
	        for (int i = 1; i < lenght - 1; i++)
	        {
	            int index = i;
	            for (int j = i + 1; j < lenght; j++)
	                if ((double) numbers.get(j).intlist.get(indexfeature)< (double)numbers.get(index).intlist.get(indexfeature)) {
	                    index = j;
	            		Collections.swap(numbers,index, i);
	                }
	            }
	        long last =  System.currentTimeMillis()-first;
	        System.out.println("--> Selection Sort Algotihm \n--> Milisecond : "+last +"\n--> Second : "+last/1000+"\n--> Minute : "+last/60000+"\n-------------------------");
	        
	        
	    }
	
	 
	 
	 
	  public static void bubble_srt(ArrayList<list> array,String ns) {
		  
		  	ArrayList<list> numbers = new ArrayList<list>(array);
			Collections.copy(numbers, array);
			int index = Integer.parseInt(ns);
	        int n = array.size();
	        int k;
	        long first =  System.currentTimeMillis();
	 
	        for (int m = n; m >= 1; m--) {
	            for (int i = 1; i < n - 1; i++) {
	                k = i + 1;
	                if ((double)numbers.get(i).intlist.get(index) >(double) numbers.get(k).intlist.get(index)) {
	                    Collections.swap(numbers, i, k);
	                }
	            }
	          
	        }
	        long last =  System.currentTimeMillis()-first;
	        System.out.println("--> Bubble Sort Algotihm \n--> Milisecond : "+last +"\n--> Second : "+last/1000+"\n--> Minute : "+last/60000+"\n-------------------------");
		        
	    }
	  
	 
	  
	  
	public static void main (String[] args ) {
	
		reader(args[0]);
		insertionSort(args[0],twoarray,args[1],args[2]);
		SelectionSort(twoarray,args[1]);
		bubble_srt(twoarray,args[1]);
	
    
	}

}




